ORDER_REQUEST_DATA = '''{
  "order": {
    "line_items": [
      {
        "variant_id": $variant_id,
        "quantity": $quantity
      }
    ]
  }
}'''

ORDER_FEATURE = 'orders'

ORDER_CONFIRMATION_CONTEXT = {'store_name': 'store_mpay_demo', 'item_list': []}

LOCATION_FEATURE = 'locations'
INVENTORY_FEATURE = 'inventory_levels/adjust'
HTTP_ERR_RESPONSE = 'Something Wrong Happened. Please try again later!'
NO_STORE_LOCATION = 'No Store Location Found. Aborting..!'

INVENTORY_REQUEST_DATA = '''{
  "location_id": $location_id,
  "inventory_item_id": $inventory_id,
  "available_adjustment": $adjustment
}'''

INV_UPDATE_FAILED = 'Inventory Update Failed. Please try again later!'