import json, requests
from string import Template
from django.conf import settings
from .constants import *
from django import template
from product.utils import ShopifyProductExtract



''' Class to Create Order; Inherits ShopifyProductExtract
    and overrides its _send_request method;
    Inventory Update method is also implemented
'''
class ShopifyCreateOrder(ShopifyProductExtract):

    def __init__(self, **kwargs):
        pass

    def _prepare_order_request_data(self, variant_id, quantity):
        request_str = Template(ORDER_REQUEST_DATA).substitute(
            variant_id=variant_id, quantity=quantity)
        return request_str

    def _get_store_location(self):
        URL = self._prepare_url(LOCATION_FEATURE) #prepare URL for getting store location
        response = requests.get(url=URL)
        if response.status_code != 200:
            raise Exception(HTTP_ERR_RESPONSE)
        response_text = json.loads(response.text)
        if not LOCATION_FEATURE in response_text:
            raise Exception(NO_STORE_LOCATION)
        for location in response_text[LOCATION_FEATURE]:
            if 'id' in location:
                return location['id']
        return None

    def _send_request(self, feature, variant_id, quantity=1):
        to_return = {}
        URL = self._prepare_url(feature)
        request_str = self._prepare_order_request_data(variant_id, quantity)
        request_str = json.loads(request_str)
        response = requests.post(url=URL, json=request_str)
        if response.status_code != 201:
            raise Exception(HTTP_ERR_RESPONSE)
        response_text = json.loads(response.text)
        return to_return

    def _update_inventory(self, inventory_id, adjustment=-1):
        location_id = self._get_store_location()
        if location_id is None:
            raise Exception(NO_STORE_LOCATION)
        request_str = Template(INVENTORY_REQUEST_DATA).substitute(
            location_id=location_id, inventory_id=inventory_id,
            adjustment=adjustment)
        URL = self._prepare_url(INVENTORY_FEATURE)
        request_str = json.loads(request_str)
        response = requests.post(url=URL, json=request_str)
        if response.status_code != 200:
            raise Exception(INV_UPDATE_FAILED)
        return