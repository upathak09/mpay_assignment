from django.conf.urls import url, include
from .views import place_order, order_confirmation

urlpatterns = [
    url(r'^ajax/place-order', place_order),
    url(r'^order-confirmation', order_confirmation),
    
]
