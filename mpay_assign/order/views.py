# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect

from datetime import datetime

from .utils import ShopifyCreateOrder

from .constants import *

def place_order(request):
    if request.method == 'POST':
        s = ShopifyCreateOrder()
        variant = request.POST['variant_data'].split("@") 
        inventory_item_id = variant[0]
        variant_id = variant[-1]
        context = s._send_request(ORDER_FEATURE, variant_id)
        s._update_inventory(inventory_item_id)
        return redirect('/order/order-confirmation/', permanent=True)


def order_confirmation(request):	
	if request.method == 'GET':
		context = ORDER_CONFIRMATION_CONTEXT
        datetime_now = datetime.now()
        context.update({'order_date': datetime_now})
        return render(request, 'order_confirmation.html', context)
