# from .models import *
import json, requests
# from string import Template
from django.conf import settings
from .constants import *
from django import template

class ShopifyProductExtract(object):

    def __init__(self, **kwargs):
        pass

    def _prepare_url(self, feature):
        URL = "https://" + settings.SHOPIFY_API_KEY + ":" + \
            settings.SHOPIFY_PASSWORD + "@" + settings.SHOPIFY_STORE_NAME + \
            settings.SHOPIFY_URL
        URL += feature + '.json'
        return URL

    def _send_request(self, feature):
        URL = self._prepare_url(feature)
        response = requests.get(URL)
        if response.status_code != 200:
            raise Exception(HTTP_ERR_RESPONSE) 
        response_text = json.loads(response.text)
        if not 'products' in response_text or not response_text['products']:
            raise Exception(NO_PRODUCTS_ADDED)
        return self._modify_response(response_text)
    
    def _modify_response(self, response_text):
        to_return = {}
        to_return['products'] = []
        for product in response_text['products']:
            product_dict = {}
            product_dict['handle'] = product['handle']
            product_dict['title'] = product['title']
            for variant in product['variants']:
                if 'price' in variant:
                    product_dict['price'] = variant['price']
                if 'old_inventory_quantity' in variant:
                    product_dict['cur_inventory'] = variant[
                        'old_inventory_quantity']
                if 'id' in variant:
                    product_dict['variant_id'] = variant['id']
                if 'inventory_item_id' in variant:
                    product_dict['variant_data'] = str(variant[
                        'inventory_item_id']) + '@' + str(variant['id'])
            for image in product['images']:
                product_dict['image_src'] = image['src'] # only 1 image being displayed on product page
            to_return['products'].append(product_dict)
        return to_return