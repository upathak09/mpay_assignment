# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from django.http import HttpResponseRedirect
from django.contrib.auth.views import LoginView
from django.core.urlresolvers import reverse
from django import forms
from .forms import UserRegistrationForm
from .utils import ShopifyProductExtract
from .constants import *


# Create your views here.
def home(request):
    return render(request, 'home.html')

def register(request):
    if request.method == 'POST':
        form = UserRegistrationForm(request.POST)
        if form.is_valid():
            userObj = form.cleaned_data
            username = userObj['username']
            email =  userObj['email']
            password =  userObj['password']
            if not (User.objects.filter(
                    username=username).exists() or User.objects.filter(
                    email=email).exists()):
                User.objects.create_user(username, email, password)
                user = authenticate(username = username, password = password)
                login(request, user)
                return HttpResponseRedirect('/')
            else:
                raise forms.ValidationError(
                'Looks like a username with that email or password already exists')
    else:
        form = UserRegistrationForm()
    return render(request, 'register.html', {'form' : form})


def product_display(request):
    if request.method == 'GET':
        s = ShopifyProductExtract()
        context = s._send_request(PRODUCTS_FEATURE)
        return render(request, 'product.html', context)


class MyLoginView(LoginView):

    def get_success_url(self):
        return reverse(
            'product-display')