from django.conf.urls import url, include
from django.contrib import admin
from .views import home, register, product_display
urlpatterns = [
    url(r'^product-display/', product_display, name='product-display'),
]
