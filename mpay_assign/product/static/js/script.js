$(document).ready(function() {

  $('.color-choose input').on('click', function() {
      var image = $(this).attr('data-image');

      $('.active').removeClass('active');
      $('.left-column img[data-image = ' + image + ']').addClass('active');
      $(this).addClass('active');
  });

});

